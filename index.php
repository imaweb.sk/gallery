<!DOCTYPE html>
<html lang="en">
<head>
	
	<!-- Page title -->
	<title>Gallery</title>

	<!-- Meta tags -->
	<meta charset="UTF-9">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Bootstrap -->
	<link 
		rel="stylesheet"
		href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
		integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
		crossorigin="anonymous"
	>

	<!-- Style -->
	<link
		rel="stylesheet" 
		href="css/style.css"
	>

	<!-- Icons -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.14.0/css/all.css" integrity="sha384-HzLeBuhoNPvSl5KYnjx0BT+WB0QEEqLprO+NBkkk5gbc67FTaL7XIGa2w1L0Xbgc" crossorigin="anonymous">

	<!-- Manifest -->
	<link rel="manifest" href="/manifest.json">
	
	<!-- Favicon -->
	<link rel="icon" type="image/png" href="/favicon.png"/>

	<script
		src="https://code.jquery.com/jquery-3.5.1.min.js"
		integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
		crossorigin="anonymous"
	></script>

</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-4 col-lg-3 col-xl-2 sidebar-holder">
				<div class="albums">
					<a href="/" class="all">
						<span>Galleries</span>
					</a>
					<ul>
						<li class="bt-all">
							<a href="/">
								<span class="mr-1"><i class="fas fa-camera-retro"></i></span>
								<span>All galleries</span>
							</a>
						</li>
						<?php 
							if(count(scandir(__DIR__ . "/galleries")) <= 2) {
								?>
									<p>No galleries available</p>
								<?php
							} else {
								foreach(scandir(__DIR__ . "/galleries") as $gallery) {
									if (!in_array($gallery,array(".",".."))) { ?>
										<li>
											<a href="?gallery=<?php echo $gallery ?>">
												<span class="mr-1">
													<?php if(isset($_GET['gallery']) && $gallery == $_GET['gallery']) { ?>
														<i class="fas fa-folder-open"></i>
													<?php } else { ?>
														<i class="fas fa-folder"></i>
													<?php } ?>
												</span>
												<?php echo $gallery ?>
											</a>
										</li>
									<?php } 
								}
							}
						?>
					</ul>
				</div>
			</div>
			<div class="offset-lg-3 offset-xl-2 col-md-12 col-lg-9 col-xl-10 right-side">
				<div class="border-bottom p-2 gallery-title">
					<?php 
						if(isset($_GET['gallery'])) {
							echo '<a href="/" class="back"><i class="fas fa-arrow-left"></i></a>';
							echo "<span class='page'>" . $_GET['gallery'] . "</span>";
						} else {
							echo "Galleries";
						}
					?>
				</div>
				<div class="row images">
					<?php 
						if(count(scandir(__DIR__ . "/galleries")) <= 2) {
							?>
								<p class="no-galleries">No galleries available</p>
							<?php
						} else {
							if(isset($_GET['gallery'])) {
								if(count(scandir(__DIR__ . "/galleries/" . $_GET['gallery'])) <= 2) {
									?>
										<div class="pt-5 w-100">
											<p class="w-100 mt-5 mb-0 text-center" style="font-size: 50px;"><i class="far fa-folder-open"></i></p>
											<p class="text-center w-100">Upload something!</p>
										</div>
									<?php
								} else {
									foreach (scandir(__DIR__ . "/galleries/" . $_GET['gallery']) as $key => $value) {
										if (!in_array($value,array(".","..","000-thumbnail.png"))) { ?>
											<?php if (
												strpos($value, '.png') !== false
												|| strpos($value, '.jpg') !== false
											) { ?> 
											<div class="col-xs-6 col-sm-6 col-md-4 col-lg-3 col-xl-2 image-holder">
												<div
													class="img-fluid img-opener"
													style="
														height: 200px;
														background-image: url('<?php echo "galleries/" . $_GET['gallery'] . "/" . $value; ?>');
														background-size: cover;
														background-position: center;
													"
													src="<?php echo "galleries/" . $_GET['gallery'] . "/" . $value; ?>"
												></div>
											</div>
										<?php } }
									} 
								}
							} else {
								foreach(scandir(__DIR__ . "/galleries") as $gallery) {
									if (!in_array($gallery,array(".",".."))) { 
										if(
											file_exists(__DIR__ . "/galleries/" . $gallery . "/thumbnail")
											&& scandir(__DIR__ . "/galleries/" . $gallery . "/thumbnail")[2]
										){ 
											$thumbnail = "/galleries/" . $gallery . "/thumbnail/" . scandir(__DIR__ . "/galleries/" . $gallery . "/thumbnail")[2] . "?cache=" . microtime();
										} else {
											
											$thumbnail = "/images/nothing.svg";
										}
										?>
											<div class="col-xs-6 col-sm-6 col-md-4 col-lg-3 col-xl-2 image-holder">
												<a href="<?php echo "?gallery=" . $gallery ?>">
													<div
														style="
															height: 200px;
															background-image: url('<?php echo $thumbnail ?>');
															background-size: cover;
															background-position: center;
														"
														src="<?php echo "galleries/" . $gallery . "/" . $value; ?>"
													></div>
												</a>
											</div>
										<?php
									}
								}
							}
						}
					?>
				</div>
			</div>		
		</div>
	</div>

	<div class="image-modal">
		<img class="opened-image" src="<?php echo "galleries/" . $_GET['gallery'] . "/" . $value; ?>">
	</div>

	<script 
		src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
		integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
		crossorigin="anonymous"
	></script>

	<script 
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
		integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
		crossorigin="anonymous"
	></script>

	<script>
		$(".img-opener").click(function(){
			$(".opened-image").attr('src', $(this).attr('src'));
			$(".image-modal").fadeIn();
		});
		$(".image-modal").click(function(){
			$(".image-modal").fadeOut();
		});
	</script>

	<script>

		var clicked = null;
		var timeoutId = 0;
		var image = "";

		$('.img-opener').on('mousedown', function() {
			clicked = $(this);
			image = $(this).attr('src');
		    timeoutId = setTimeout(setCover, 1000);
		}).on('mouseup mouseleave', function() {
		    clearTimeout(timeoutId);
		});
	 
		function setCover() {
			$.post("thumbnail.php",
			{
				image: image,
			},
			function(data, status){
				alert("New cover picture has been set for this album!");
			});
		}
		
	    document.onkeydown = function(e) {
	    	if ($('.opened-image').is(":visible")) {
			    switch(e.which) {
			        case 37: // left
			        if(clicked.parent().prev().children().length) {
			        	clicked.parent().prev().children().click();
			        	clicked = clicked.parent().prev().children();
			        }
			        break;
			        case 39: // right
			        if(clicked.parent().next().children().length) {
			        	clicked.parent().next().children().click();
			        	clicked = clicked.parent().next().children();
			        }
			        break;
			        default: return; // exit this handler for other keys
			    }
			    e.preventDefault(); // prevent the default action (scroll / move caret)
			}
		};

		jQuery(document).on('keyup',function(evt) {
		    if (evt.keyCode == 27) {
		       $(".image-modal").fadeOut();
		    }
		});

		$("form").change(function(){
			$(this).submit();
		});
		
	</script>

	<script>
		navigator.serviceWorker.register('/sw.js', { scope: '/' }).then(function (registration) {
			// console.log('Service worker registered successfully');
		}).catch(function (e) {
			// console.error('Error during service worker registration:', e);
		});
	</script>


</body>
</html>